"""Test for Mapillary."""

from app import app, db
from flask import render_template, request, redirect, url_for, flash, jsonify
from app.forms import UserForm
from app.models import User
import json
from sqlalchemy.exc import IntegrityError


@app.route('/')
def home():
    """Render website's home page."""
    return render_template('home.html')


@app.route('/about/')
def about():
    """Render the website's about page."""
    return render_template('about.html', name="Mary Jane")


@app.route('/users')
def show_users():
    """Retrieve all users on UI"""
    users = db.session.query(User).all()

    return render_template('show_users.html', users=users)


@app.route('/users-json', methods=['GET'])
def get_json_users():
    """Retrieve all users in json."""
    users = db.session.query(User).all()
    response = []
    for user in users:
        response.append(user.to_json())
    return jsonify(response)


@app.route('/add-user-json', methods=['POST'])
def add_user_json():
    resp = None
    data = request.get_json()
    print('pass 1')
    print(data)
    username = data['username']
    password = data['password']
    birth_date = data['birth_date']
    email = data['email']
    address = data['address']
    user = User(username, password, birth_date, email, address)
    users = db.session.query(User).all()
    try:
        db.session.add(user)
        db.session.commit()
        resp = 'true'

    except IntegrityError:
        resp = 'Maybe there is already a user with this e-mail.'

    return resp

@app.route('/add-user', methods=['POST', 'GET'])
def add_user():
    """Add an user on the system using web interface."""
    user_form = UserForm()

    if request.method == 'POST':
        if user_form.validate_on_submit():
            username = user_form.username.data
            password = user_form.password.data
            birth_date = user_form.birth_date.data
            email = user_form.email.data
            address = user_form.address.data

            # save on database

            user = User(username, password, birth_date, email, address)
            db.session.add(user)
            db.session.commit()

            flash('User successfully added')
            return redirect(url_for('show_users'))

    flash_errors(user_form)
    return render_template('add_user.html', form=user_form)


def flash_errors(form):
    """Flash errors from the form if validation fails"""
    for field, errors in form.errors.items():
        for error in errors:
            flash(u"Error in the %s field - %s" % (
                getattr(form, field).label.text,
                error
            ))


@app.route('/<file_name>.txt')
def send_text_file(file_name):
    """Send your static text file."""
    file_dot_text = file_name + '.txt'
    return app.send_static_file(file_dot_text)


@app.after_request
def add_header(response):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame
    and also to cache the rendered page for 10 minutes.
    """
    response.headers['X-UA-Compatible'] = 'IE=Edge,chrome=1'
    response.headers['Cache-Control'] = 'public, max-age=600'
    return response


@app.errorhandler(404)
def page_not_found(error):
    """Custom 404 page."""
    return render_template('404.html'), 404


if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0", port="8080")
